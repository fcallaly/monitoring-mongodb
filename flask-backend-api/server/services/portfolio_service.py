import logging
import os

import pymongo

from flask import current_app

LOG = logging.getLogger(__name__)


def init_db():
    mongo_host = os.getenv('MONGO_HOST', 'localhost')
    mongo_port = os.getenv('MONGO_PORT', 27017)
    finance_db = os.getenv('FINANCE_DB', 'financedb')
    portfolio_collection = os.getenv('PORTFOLIO_COLLECTION', 'portfolio')

    if(hasattr(current_app, '_mongodb') and
       hasattr(current_app, '_db_collection')):
        return

    LOG.info('Initialising MongoDB resource: ' +
             str(mongo_host) + ':' + str(mongo_port))

    current_app._mongodb = pymongo.MongoClient(host=mongo_host,
                                               port=mongo_port,
                                               connectTimeoutMS=4000,
                                               serverSelectionTimeoutMS=4000)
    current_app._db_collection = \
        current_app._mongodb[finance_db][portfolio_collection]


def get_entries(limit=10, esk=None, symbol=None):
    LOG.debug('get entries')

    filter = {}
    if symbol:
        filter['symbol'] = symbol

    if esk:
        response = list(current_app._db_collection.find(filter))
    else:
        response = list(current_app._db_collection.find(filter))

    LOG.debug('Received ' + str(response) + ' from DB')

    return response


def create_entry(portfolio_resource):
    LOG.debug('create: ' + str(portfolio_resource))

    response = current_app._db_collection.insert_one(portfolio_resource)

    LOG.debug('Received ' + str(response) + ' from DB')

    if response is None:
        LOG.debug('Error creating new Portfolio Item')
        raise Exception('Error creating new Portfolio Item: ' +
                        str(portfolio_resource))

    return response.inserted_id
