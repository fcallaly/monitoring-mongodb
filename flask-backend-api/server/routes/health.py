from flask import jsonify
from flask_restx import Namespace, Resource
from server import app, api

health_namespace = Namespace('health', description='Return Application Health')

api.add_namespace(health_namespace)


@health_namespace.route("")
class Health(Resource):

    def get(self):
        state = {"status": "UP"}
        return jsonify(state)
