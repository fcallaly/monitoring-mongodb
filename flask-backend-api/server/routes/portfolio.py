import json
import logging

from flask import jsonify, request, Response
from flask_restx import abort, Namespace, Resource, fields
from server import app, api

from server.services import portfolio_service


LOG = logging.getLogger(__name__)

portfolio_namespace = Namespace('portfolio',
                            description='Interface for Portfolio Resource')

api.add_namespace(portfolio_namespace)

portfolio_fields = api.model('PortfolioItem',
                         {
                            'type': fields.String,
                            'symbol': fields.String,
                            'price': fields.Float,
                            'quantity': fields.Integer,
                            'date': fields.Date
                         })


@portfolio_namespace.route("")
class Portfolio(Resource):

    @api.param('symbol', description='item symbol', type='string')
    def get(self):
        response = portfolio_service.get_entries(symbol=request.args.get("symbol"))

        if response is None:
            abort(404, message='Entries not found for symbol: ' +
                               str(request.args.get("symbol")))
        return Response(json.dumps(response, default=str),
                        mimetype="application/json")

    @api.expect(portfolio_fields)
    def post(self):
        LOG.debug('Create new Portfolio Entry: ' + str(api.payload))
        try:
            portfolio_service.create_entry(api.payload)
        except Exception as e:
            return {'success': False, 'message': str(e)}, 400

        return {'success': True}, 201
