import os
from flask import Blueprint, Flask, abort, session, request, redirect
from flask_wtf.csrf import CSRFProtect
from flask_restx import Resource, Api, fields
from flask.json import jsonify

from server.services import portfolio_service
from prometheus_flask_exporter import PrometheusMetrics

app = Flask(__name__, template_folder="../public",
            static_folder="../public", static_url_path='')

app.config['ERROR_404_HELP'] = False
# csrf = CSRFProtect(app)

blueprint = Blueprint('api', __name__, url_prefix='/v1')
api = Api(blueprint,
          title="My API",
          version='v0.1',
          description='Description'
          )
app.register_blueprint(blueprint)

from server.routes import *  # noqa

app.before_first_request(portfolio_service.init_db)


@app.after_request
def after_request(response):
    response.headers.add('Access-Control-Allow-Origin', '*')
    response.headers.add('Access-Control-Allow-Headers', '*')
    return response

metrics = PrometheusMetrics(app)
