#! /bin/bash

mongoimport --host mongodb --db financedb --collection portfolio --type json --file /mongo-seed/portfolio.json --jsonArray
