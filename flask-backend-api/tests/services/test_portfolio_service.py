import pymongo
import pytest

from server.services import portfolio_service


class DummyAppContext:
    pass


@pytest.fixture
def dummy_app():
    dummy_app = DummyAppContext()
    portfolio_service.current_app = dummy_app
    return dummy_app


def test_init_db(dummy_app, mocker):
    mocker.patch('pymongo.MongoClient')

    portfolio_service.init_db()

    pymongo.MongoClient.assert_called_once()


def test_get_all(dummy_app, mocker):
    items = [{'_id': '123abc', 'symbol': 'GOOG', 'quantity': 100},
             {'_id': '345f2', 'symbol': 'AAPL', 'quantity': 23}]

    dummy_app._db_collection = mocker.MagicMock()
    dummy_app._db_collection.find.return_value = items

    assert portfolio_service.get_entries() == items


def test_get_entry_by_symbol(dummy_app, mocker):
    item = {'_id': '123abc', 'symbol': 'GOOG', 'quantity': 100}
    dummy_app._db_collection = mocker.MagicMock()
    dummy_app._db_collection.find.return_value = item

    assert portfolio_service.get_entries('GOOG') == item


def test_create_entry(dummy_app, mocker):
    mocker.patch('server.services.portfolio_service.get_entries',
                 return_value=None)

    insert_id = 'abcd2345'
    dummy_app._db_collection = mocker.MagicMock()
    dummy_app._db_collection.insert_one.return_value = \
        pymongo.results.InsertOneResult(insert_id, True)

    assert portfolio_service.create_entry({'symbol': 'AAPL'}) == insert_id
