
import json
import logging
import pytest
import urllib.parse

from server import app

LOG = logging.getLogger(__name__)

app.before_first_request_funcs = []


@pytest.fixture
def app_client():
    app_client = app.test_client()
    app_client.testing = True
    yield(app_client)


def test_root_endpoint(app_client):
    response = app_client.get('/')
    assert response.status_code == 200
    response.close()


def test_health_endpoint(app_client):
    result = json.loads(app_client.get('/v1/health').data)
    LOG.debug('/v1/health : ' + str(result))
    assert result['status'] == 'UP'


def test_portfolio_endpoint(app_client, mocker):
    mocker.patch('server.services.portfolio_service.get_entries',
                 return_value={'id': 199, 'symbol': 'AAPL'})

    url = '/v1/portfolio?symbol=' + urllib.parse.quote_plus('AAPL')
    result = json.loads(app_client.get(url).data)
    LOG.debug(str(url) + ' : ' + str(result))

    assert result['id'] == 199
