import time
import random
import threading

import requests

portfolio_endpoints = ('v1/health', 'v1/portfolio?symbol=GOOG', 'notfound')
frontend_endpoints = ('', 'index.html', 'test.html', 'nonexistant.html')

def run():
    while True:
        try:
            target = random.choice(portfolio_endpoints)
            requests.get("http://portfolio-app:5000/%s" % target, timeout=1)
            time.sleep(2)
            target = random.choice(frontend_endpoints)
            requests.get("http://frontend-app:5000/%s" % target, timeout=1)
            time.sleep(5)
        except:
            pass


if __name__ == '__main__':
    for _ in range(4):
        thread = threading.Thread(target=run)
        thread.setDaemon(True)
        thread.start()

    while True:
        time.sleep(1)
