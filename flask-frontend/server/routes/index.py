from server import app

@app.route('/')
def static_file():
    return app.send_static_file('index.html')
