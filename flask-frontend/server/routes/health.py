from flask import jsonify
from server import app


@app.route("/health")
def get_health():
    state = {"status": "UP"}
    return jsonify(state)
