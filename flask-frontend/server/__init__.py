from flask import Flask

from prometheus_flask_exporter import PrometheusMetrics

app = Flask(__name__, template_folder="../public",
            static_folder="../public", static_url_path='')

app.config['ERROR_404_HELP'] = False

from server.routes import *  # noqa

metrics = PrometheusMetrics(app)
