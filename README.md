# Dashboard Sandbox
This project is designed to be a sandbox environment in which you can experiment with Prometheus and Grafana.

The included docker-compose.yml file can be used to bring up all of the components as docker containers. This requires docker and docker-compose to be installed.

```shell
$ docker-compose up
```

The main components of the demo application are:
1. A mongodb database for storing data.
2. A "backend" REST API which stores it's data in mongodb. This uses Python/Flask.
3. A "frontend" server which serves static files e.g. HTML, Javascript, CSS. This also uses Python/Flask.
4. A simple traffic generator. This sends HTTP requests to the frontend and backend apps.
5. A script that loads some initial data into mongodb.

The other components included for monitoring are:
1. The mongodb prometheus exporter. This allows prometheus to read metrics from mongodb.
2. The prometheus node exporter. This allows prometheus to read metrics from the host machine e.g. CPU, memory.
3. A prometheus server configured to read metrics from
    - mongodb
    - the frontend app
    - the backend app
    - performance metrics from the host machine.
3. A grafana server configured to read metrics from the prometheus server. This includes two demo dashboards:
    - A mongodb dashboard
    - A "HTTP Apps" dashboard - designed for the frontend and backend flask apps.

## Network access
The following components will be externally accessible if their ports are open (e.g. open these ports in a firewall / Security Group)

| Component | Port |
|-----------| -----|
| frontend  | 80   |
| backend   | 5000 |
| prometheus| 9090 |
| grafana   | 3000 |

## Structure
![Project Structure](microservice-monitoring-demo.PNG)

## Starting/Stopping components with docker-compose
You can start/stop some or all of the components with docker-compose, some example useful docker-compose commands are:

Bring up everything:
```shell
$ docker-compose up
```

Bring up a subset of what's listed in docker-compose.yml:
```shell
$ docker-compose up mongodb portfolio-app prometheus grafana
```

Stop an individual component (e.g. mongodb):
```shell
$ docker-compose stop mongodb
```

Start an individual component (e.g. mongodb):
```shell
$ docker-compose start mongodb
```

Check what's running:
```shell
$ docker-compose ps
```

## HTTP Apps dashboard

![HTTP Apps dashboard](dashboard.png)

With this example dashboard, you can view basic metrics on the two flask apps.

Then open [http://<host>:3000](http://localhost:3000) in your browser to see the grafana console. You can navigate to the HTTP Apps dashboard.

You can edit each panel to check what metric it uses, but here is a quick rundown of what's going on in there.

### Requests per second

Number of successful Flask requests per second. Shown per path.

```
rate(
  flask_http_request_duration_seconds_count{status="200"}[30s]
)
```

### Errors per second

Number of failed (non HTTP 200) responses per second.

```
sum(
  rate(
    flask_http_request_duration_seconds_count{status!="200"}[30s]
  )
)
```

### Total requests per minute

The total number of requests measured over one minute intervals. Shown per HTTP response status code.

```
increase(
  flask_http_request_total[1m]
)
```

### Average response time [30s]

The average response time measured over 30 seconds intervals for successful requests. Shown per path.

```
rate(
  flask_http_request_duration_seconds_sum{status="200"}[30s]
)
 /
rate(
  flask_http_request_duration_seconds_count{status="200"}[30s]
)
```

### Requests under 250ms

The percentage of successful requests finished within 1/4 second. Shown per path.

```
increase(
  flask_http_request_duration_seconds_bucket{status="200",le="0.25"}[30s]
)
 / ignoring (le)
increase(
  flask_http_request_duration_seconds_count{status="200"}[30s]
)
```

### Request duration [s] - p50

The 50th percentile of request durations over the last 30 seconds. In other words, half of the requests finish in (min/max/avg) these times. Shown per path.

```
histogram_quantile(
  0.5,
  rate(
    flask_http_request_duration_seconds_bucket{status="200"}[30s]
  )
)
```

### Request duration [s] - p90

The 90th percentile of request durations over the last 30 seconds. In other words, 90 percent of the requests finish in (min/max/avg) these times. Shown per path.

```
histogram_quantile(
  0.9,
  rate(
    flask_http_request_duration_seconds_bucket{status="200"}[30s]
  )
)
```

### Memory usage

The memory usage of the Flask app. Based on data from the underlying Prometheus client library, not Flask specific.

```
process_resident_memory_bytes{job="example"}
```

### CPU usage

The CPU usage of the Flask app as measured over 30 seconds intervals. Based on data from the underlying Prometheus client library, not Flask specific.

```
rate(
  process_cpu_seconds_total{job="example"}[30s]
)
```
